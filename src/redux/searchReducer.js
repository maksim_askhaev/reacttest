import React from "react";

const SET_CATEGORIES = 'SET_CATEGORIES';
const SET_SEARCH_STRING = 'SET_SEARCH_STRING';

let initialState = {
    categoriesExternal: [
        {id:1, name: 'Монитор 1', types: ["тип1", "тип2"]},
        {id:2, name: 'Системный блок',  types: ["тип1", "тип2", "тип3"]},
        {id:3, name: 'Клавиатура',  types: ["тип1"]},
        {id:4, name: 'Монитор 2',  types: ["тип2"]},
        {id:5, name: 'Монитор 3',  types: ["тип3"]},
        {id:6, name: 'Клавиатура',  types: ["тип2", "тип3"]}
    ],
    categories: [],
    searchString: ""
};

const searchReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_CATEGORIES:
            return {
                ...state,
                categories: [...action.categories]
            };
        case SET_SEARCH_STRING:
            return {
                ...state,
                searchString: action.searchString
            };
        default:
            return state;
    }
}

export const setCategoriesAC = (categories) => ({type: SET_CATEGORIES, categories });

export const setSearchStringAC = (searchString) => ({type: SET_SEARCH_STRING, searchString });

export default searchReducer;