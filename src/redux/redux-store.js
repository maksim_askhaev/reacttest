import {combineReducers, createStore} from "redux";
import searchReducer from "./searchReducer";

/**
 * Редакс-стор. Хранит состояние приложения.
 * Вызывает рендеринг при обновлении стора.
 *
 * @type {Reducer<CombinedState<unknown>>}
 */
let reducers = combineReducers({
        searchPage: searchReducer
    }
);

let store = createStore(reducers);

export default store;

