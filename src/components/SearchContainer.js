import React from "react";
import Search from "./Search";
import {
    setCategoriesAC, setSearchStringAC
} from "../redux/searchReducer";
import {connect} from "react-redux";
import queryString from 'query-string';
import {withRouter} from "react-router-dom";

/**
 * Контейнерная компонента.
 */
class SearchContainer extends React.Component {
    componentDidMount() {
        this.searchCategories()
    };

    /**
     * Проверяет совпадение строки nameString с искомым параметром строки filterName.
     *
     * @param nameString
     * @param filterName
     * @returns {boolean}
     */
    filterByName = (nameString, filterName) => {
        return nameString === filterName;
    };

    /**
     * Проверяет наличие в массиве typesArray значений из строки filterType.
     * Возвращает true, если найдены все значения в массиве.
     *
     * @param typesArray массив значений.
     * @param filterType строка значений, разделенных запятой, которые нужно найти в массиве.
     * @returns {boolean} если все значения из строки найдены.
     */
    filterByType = (typesArray, filterType) => {
        let filterTypes = filterType.split(",");
        let result = true;

        filterTypes.forEach((type) => {
            result = result && (typesArray.indexOf(type) > -1);
        });

        return result;
    };

    /**
     * Колбэк, формирует строку поиска и вызывает поиск.
     *
     * @param e
     * @param refType1
     * @param refType2
     * @param refType3
     */
    handleInputKeyPress = (e, refType1, refType2, refType3) => {
        if (e.key === 'Enter') {
            let searchString;
            if (e.target.value) searchString = "name="+e.target.value;

            let array = [];
            const type1 = refType1.current.checked;
            const type2 = refType2.current.checked;
            const type3 = refType3.current.checked;

            if (type1 === true) array.push("тип1");
            if (type2 === true) array.push("тип2");
            if (type3 === true) array.push("тип3");

            if (array.length > 0) {
                searchString = searchString + "&type="+array.toString();
            }

            this.props.history.push("/search?"+searchString);
            window.location.reload();
        }
    };

    /**
     * Выполняет поиск согласно параметрам строки поиска.
     * Обновляет стор.
     *
     * @returns {*|void}
     */
    searchCategories() {
        const values = queryString.parse(this.props.location.search);
        const filterName = values.name;
        const filterType = values.type;

        if (filterName || filterType) {
            return this.props.setCategories(this.props.categoriesExternal.filter((category) => {
                let result = true;
                if (filterName) result = this.filterByName(category.name, filterName);
                if (filterType) result = result && this.filterByType(category.types, filterType);

                return result;
            }));
        } else {
            return this.props.setCategories(this.props.categoriesExternal);
        }
    };

    setSearchString = (text) => {
        this.props.setSearchString(text);
    }

    render() {
        return (
            <div>
                <Search
                    categories={this.props.categories}
                    handleInputKeyPress = {this.handleInputKeyPress}
                    searchString={this.props.searchString}
                    setSearchString={this.props.setSearchString}
                />
            </div>
        )
    }
}

let mapStateToProps = (state) => {
    return {
        categories: state.searchPage.categories,
        categoriesExternal: state.searchPage.categoriesExternal,
        searchString: state.searchPage.searchString
    }
};

let mapDispatchToProps = (dispatch) => {
    return {
        setCategories: (categories) => {
            dispatch(setCategoriesAC(categories))
        },
        setSearchString: (searchString) => {
            dispatch(setSearchStringAC(searchString))
        }
    }
};

const MySearchContainer = withRouter(connect(mapStateToProps, mapDispatchToProps)(SearchContainer));

export default MySearchContainer;