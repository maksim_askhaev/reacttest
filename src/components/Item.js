import React from "react";
import styles from "./Item.module.css"

const Item = (props) => {

    return (
        <div className={styles.item}>
            <div className={styles.name}>
                {props.category.name}
            </div>
            <div className={styles.checkboxes}>
                {
                    props.category.types.map(type => (
                        <label> {type} </label>
                    ))
                }
            </div>
        </div>
    )
};

export default Item;