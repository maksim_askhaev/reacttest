import React, {createRef} from "react";
import styles from "./Search.module.css"
import Item from "./Item";

/**
 * Презентационная (чистая) компонента.
 */
class Search extends React.Component {
    checkbox1 = createRef();
    checkbox2 = createRef();
    checkbox3 = createRef();

    onHandleInputKeyPress = (e) => {
        this.props.handleInputKeyPress(e, this.checkbox1, this.checkbox2, this.checkbox3);
    };

    onSetSearchString = (e) => {
        this.props.setSearchString(e.target.value);
    }

    render() {
        return (
            <div className={styles.searchBlock}>
                <input
                    className={styles.input}
                    type="text"
                    placeholder={"Поиск"}
                    onKeyPress={this.onHandleInputKeyPress}
                    ref={input => input && input.focus()}
                    value={this.props.searchString}
                    onChange={this.onSetSearchString}
                />

                <div className={styles.options}>
                    <div className={styles.block}>
                        <div className={styles.myCheckbox}>
                            <input name="type1"
                                   type="checkbox"
                                   ref={this.checkbox1}
                            />
                            <label> Тип 1</label>
                        </div>
                    </div>
                    <div className={styles.block}>
                        <div className={styles.myCheckbox}>
                            <input name="type2" type="checkbox" ref={this.checkbox2}/>
                            <label> Тип 2</label>
                        </div>
                    </div>
                    <div className={styles.block}>
                        <div className={styles.myCheckbox}>
                            <input name="type3" type="checkbox" ref={this.checkbox3}/>
                            <label> Тип 3</label>
                        </div>
                    </div>
                </div>
                <div className={styles.searchResults}>
                    {
                        this.props.categories.length === 0
                            ? <span>Не найдено</span>
                            : this.props.categories.map(category => (
                            <Item category={category}/>
                        ))
                    }
                </div>
            </div>
        )
    }
}

export default Search;

