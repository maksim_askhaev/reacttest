import React from 'react';
import styles from "./App.module.css";
import SearchContainer from "./components/SearchContainer";
import {Redirect, Route} from "react-router-dom";

function App() {
    return (
        <div className={styles.App}>
            <Route exact path='/' render={()=> (<Redirect to="/search"/>)}/>
            <Route path='/search' component={SearchContainer}/>
        </div>
    );
}

export default App;
